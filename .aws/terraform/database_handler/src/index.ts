import type { APIGatewayProxyEventV2, APIGatewayProxyResultV2 } from 'aws-lambda'
import queryString from 'query-string'
import {getItem} from './items'
import { createItem } from './createItem'
import { updateItem } from './updateItem'
import { deleteItem } from './deleteItem'
import { getAllItems } from './getAllItems'

export const handler = async (event: APIGatewayProxyEventV2): Promise<APIGatewayProxyResultV2> => {
    console.log(event)

    const method = event.requestContext.http.method
    const paths = event.rawPath.split('/').filter(x => x !== '')
    const {
        limit = undefined,
        from = undefined,
    } = queryString.parse(event.rawQueryString, {parseNumbers: true});

    console.log(JSON.stringify({method, paths, limit, from}))

    // test the api
    // GET /api/health
    if (method === 'GET' && paths[1] === 'health') {
        return {
            headers: {"Content-Type": "application/json"},
            statusCode: 200,
            body: JSON.stringify({ message: 'Ok' }),
        }
    }

    // get a specific object in the table
    // GET /api/image/:id
    if (method === 'GET' && paths[1] === 'image' && paths[2]) {
        console.log('GET /api/image/:id')
        return getItem(paths[2])
    }

    //delete a specific object in the table
    // DELETE /api/image/:id
    if (method === 'DELETE' && paths[1] === 'image' && paths[2]) {
        console.log('DELETE /api/image/:id')
        return deleteItem(paths[2])
    }

    // update a specific object in the table
    // POST /api/image/:id
    if (method === 'PUT' && paths[1] === 'image' && paths[2]) {
        console.log('PUT /api/image/:id')
        return updateItem(paths[2], event.body)
    }

    // create a new object in the table
    // PUT /api/image
    if (method === 'PUT' && paths[1] === 'image') {
        console.log('PUT /api/image')
        return createItem(event.body)
    }

    // get all objects in the table
    // GET /api/image
    if (method === 'GET' && paths[1] === 'image') {
        console.log('GET /api/image')
        return getAllItems({
            limit: limit as number|undefined,
            from: from as string|undefined
        })
    }

    return {
      headers: {"Content-Type": "application/json"},
      statusCode: 400,
      body: JSON.stringify({ message: 'Unsupported route', method, paths, event }),
    }
}
