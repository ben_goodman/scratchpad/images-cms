import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient, GetCommand } from "@aws-sdk/lib-dynamodb";
import { S3Client } from "@aws-sdk/client-s3"
import { type APIGatewayProxyResultV2 } from "aws-lambda";

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

const headers = {
    "Content-Type": "application/json",
};

export const getItem = async (id: string): Promise<APIGatewayProxyResultV2> => {
    const response = await dynamo.send(
        new GetCommand({
            TableName: TABLE_NAME,
            Key: {
                id,
            },
        })
    );

    return {
        headers,
        statusCode: 200,
        body: JSON.stringify(response.Item),
    }
}


