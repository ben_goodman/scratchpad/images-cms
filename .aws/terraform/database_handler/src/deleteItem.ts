import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DeleteObjectCommand, S3Client } from "@aws-sdk/client-s3";
import { GetCommand, DeleteCommand, DynamoDBDocumentClient } from "@aws-sdk/lib-dynamodb";
import { APIGatewayProxyResultV2 } from "aws-lambda";

const s3 = new S3Client({})
const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME
const BUCKET_NAME = process.env.BUCKET_NAME

const headers = {
    "Content-Type": "application/json",
}

export const deleteItem = async (id: string): Promise<APIGatewayProxyResultV2> => {
    // get the S3 key of the deleting item
    const itemResponse = await dynamo.send(
        new GetCommand({
            TableName: TABLE_NAME,
            Key: {
                id,
            },
        })
    )

    console.log(itemResponse.Item)

    await s3.send(
        new DeleteObjectCommand({
            Bucket: BUCKET_NAME,
            Key: (itemResponse as any).Item.key,
        })
    ).catch((err) => {
        console.log(`deleting image for item ${id} failed`)
        console.log(err)
        throw err
    })

    await dynamo.send(
        new DeleteCommand({
            TableName: TABLE_NAME,
            Key: {
                id,
            },
        })
    ).catch((err) => {
        console.log(`deleting item ${id} failed`)
        console.log(err)
        throw err
    })


    return {
        headers,
        statusCode: 200,
        body: JSON.stringify({ message: 'Item deleted' }),
    }
}