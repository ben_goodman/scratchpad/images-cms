import { DynamoDBClient, ScanCommand } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient } from "@aws-sdk/lib-dynamodb";
import { type APIGatewayProxyResultV2 } from "aws-lambda";

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

const headers = {
    "Content-Type": "application/json",
};

export interface GetAllItemsOptions {
    limit?: number
    from?: string
}

export const getAllItems = async ({
    limit,
    from
}: GetAllItemsOptions): Promise<APIGatewayProxyResultV2> => {
    const response = await dynamo.send(new ScanCommand({
        TableName: TABLE_NAME,
        Limit: limit,
        ExclusiveStartKey: from ? { id: { S: from } } : undefined
    }))

    return {
        headers,
        statusCode: 200,
        body: JSON.stringify(response),
    }
}
