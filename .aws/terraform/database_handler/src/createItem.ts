import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient, PutCommand } from "@aws-sdk/lib-dynamodb";
import { PutObjectCommand, S3Client } from "@aws-sdk/client-s3"
import { type APIGatewayProxyResultV2 } from "aws-lambda";
import { error } from "console"
import { customAlphabet } from 'nanoid'

const s3 = new S3Client({})
const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME
const BUCKET_NAME = process.env.BUCKET_NAME

const headers = {
    "Content-Type": "application/json",
};

const b64toBlob = (base64, type = 'application/octet-stream') =>
  fetch(`data:${type};base64,${base64}`).then(res => res.blob())


export const createItem = async (body?: string): Promise<APIGatewayProxyResultV2> => {
    if (body === undefined) {
        return {
            headers,
            statusCode: 400,
            body: JSON.stringify({ message: 'No body provided' }),
        }
    } else {
        try {
            const requestJSON = JSON.parse(body)

            const nanoid = customAlphabet('1234567890abcdefghijklmnopqrstuvwxyz', 14)
            const id = nanoid()

            console.log('converting image data to array buffer')
            const image = await b64toBlob(requestJSON.image)
            const objectKey = `assets/${id}/${requestJSON.title}`
            const imageBuf = Buffer.from(await image.arrayBuffer())

            await s3.send(
                new PutObjectCommand({
                    Bucket: BUCKET_NAME,
                    Key: objectKey,
                    Body: imageBuf,
                    ContentType: requestJSON.type,
                })
            ).catch((err) => {
                console.log(`uploading image for item ${id} failed`)
                console.log(err)
                throw err
            })

            await dynamo.send(
                new PutCommand({
                    TableName: TABLE_NAME,
                    Item: {
                        id,
                        title: requestJSON.title,
                        description: requestJSON.description,
                        key: objectKey,
                        type: requestJSON.type,
                        dimensions: requestJSON.dimensions,
                        dateUploaded: new Date().toISOString()
                    },
                })
            ).catch((err) => {
                console.log(`adding item ${id} to dynamo failed`)
                console.log(err)
                throw err
            })

            return {
                headers,
                statusCode: 200,
                body: JSON.stringify({ message: 'Image added.', id, key: objectKey}),
            }
        } catch(err){
            return {
                headers,
                statusCode: 500,
                body: JSON.stringify({ message: 'Failed to add image.', err: (error as any).errorMessage }),
            }
        }
    }
}