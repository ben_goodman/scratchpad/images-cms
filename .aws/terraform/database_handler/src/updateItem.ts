import { DynamoDBClient } from "@aws-sdk/client-dynamodb"
import { DynamoDBDocumentClient, UpdateCommand } from "@aws-sdk/lib-dynamodb"
import { type APIGatewayProxyResultV2 } from "aws-lambda"

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

const headers = {
    "Content-Type": "application/json",
};

export const updateItem = async (id: string, body?: string ): Promise<APIGatewayProxyResultV2> => {
    if (body === undefined) {
        return {
            headers,
            statusCode: 400,
            body: JSON.stringify({ message: 'No body provided' }),
        }
    }

    try {
        const requestJson = JSON.parse(body)

        await dynamo.send(new UpdateCommand({
            TableName: TABLE_NAME,
            Key: {
                id,
            },
            UpdateExpression: "set description = :description",
            ExpressionAttributeValues: {
                ":description": requestJson.description,
            },
            ReturnValues: "ALL_NEW",
          })
        ).catch((err) => {
            console.log(`updating item ${id} failed`)
            console.log(err)
            throw err
        })

        return {
            headers,
            statusCode: 200,
            body: JSON.stringify({ message: 'Item updated' }),
        }
    } catch (err) {
        return {
            headers,
            statusCode: 400,
            body: JSON.stringify({ message: 'Failed to update item' }),
        }
    }
}