
# TODO: temporary
output "database_handler_function_endpoint" {
  value = aws_lambda_function_url.database_handler_function_endpoint.function_url
}

output "cloudfront_default_domain" {
    value = module.website.cloudfront_default_domain
}

output "cloudfront_id" {
    value = module.website.cloudfront_id
}

output "default_s3_bucket_name" {
    value = module.website.bucket_name
}