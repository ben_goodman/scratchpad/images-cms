resource "aws_dynamodb_table" "default" {
    name             = "images-cms"
    hash_key         = "id"
    billing_mode     = "PROVISIONED"
    read_capacity    = 20
    write_capacity   = 20
    stream_enabled   = true
    stream_view_type = "NEW_IMAGE"

    point_in_time_recovery {
        enabled = true
    }

    attribute {
        name = "id"
        type = "S"
    }
}

