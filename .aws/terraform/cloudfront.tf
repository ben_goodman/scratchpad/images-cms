resource "aws_cloudfront_cache_policy" "default" {
    name        = "default-cache-policy-${random_id.cd_function_suffix.hex}-${var.project_name}-${var.resource_namespace}"
    comment     = "${var.project_name}-${var.resource_namespace}"
    default_ttl = 3600
    max_ttl     = 86400
    min_ttl     = 0
    parameters_in_cache_key_and_forwarded_to_origin {
        cookies_config {
            cookie_behavior = "all"
        }
        headers_config {
            header_behavior = "none"
        }
        query_strings_config {
            query_string_behavior = "all"
        }
        enable_accept_encoding_brotli = true
        enable_accept_encoding_gzip = true
    }
}

resource "aws_cloudfront_cache_policy" "api_cache_policy" {
    name        = "api-cache-policy-${random_id.cd_function_suffix.hex}-${var.project_name}-${var.resource_namespace}"
    comment     = "${var.project_name}-${var.resource_namespace}"
    default_ttl = 0
    max_ttl     = 5
    min_ttl     = 0
    parameters_in_cache_key_and_forwarded_to_origin {
        cookies_config {
            cookie_behavior = "all"
        }
        headers_config {
            header_behavior = "none"
        }
        query_strings_config {
            query_string_behavior = "all"
        }
        enable_accept_encoding_brotli = true
        enable_accept_encoding_gzip = true
    }
}

resource "aws_cloudfront_origin_access_identity" "images_bucket" {
    comment = "images-bucket-${var.project_name}-${var.resource_namespace}"
}

module "images_bucket" {
    source  = "gitlab.com/ben_goodman/s3-website/aws//modules/s3"
    version = "2.0.0"

    oai_arn     = aws_cloudfront_origin_access_identity.images_bucket.iam_arn
    bucket_name = "${var.project_name}-assets.${var.resource_namespace}"
    tags = {}
}

module "website" {
    source  = "gitlab.com/ben_goodman/s3-website/aws"
    version = "2.1.0"

    org          = var.resource_namespace
    project_name = var.project_name
    use_cloudfront_default_certificate = true
    default_cache_policy_id = aws_cloudfront_cache_policy.default.id
    default_response_headers_policy_id = aws_cloudfront_response_headers_policy.security_headers.id

    additional_s3_origins = {
        "images-bucket" = {
            domain_name = module.images_bucket.aws_s3_bucket.bucket_regional_domain_name
            origin_access_identity_path = aws_cloudfront_origin_access_identity.images_bucket.cloudfront_access_identity_path
        }
    }

    additional_custom_origins = {
        "images-api" = {
            domain_name = trimsuffix(trimprefix(aws_lambda_function_url.database_handler_function_endpoint.function_url, "https://"), "/")
        }
    }

    ordered_cache_behaviors = {
        "images-bucket" = {
            path_pattern = "/assets/*"
            viewer_protocol_policy = "https-only"
            cache_policy_id = aws_cloudfront_cache_policy.default.id
        },
        "images-api" = {
            path_pattern = "/api/*"
            viewer_protocol_policy = "https-only"
            cache_policy_id = aws_cloudfront_cache_policy.api_cache_policy.id
        }
    }
}

