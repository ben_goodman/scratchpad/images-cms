locals {
    database_handler_root = "${path.module}/database_handler"
}

resource "null_resource" "database_handler_function" {
    provisioner "local-exec" {
        working_dir = local.database_handler_root
        command     = "make all"
        environment = {
            TABLE_NAME  = aws_dynamodb_table.default.name
            BUCKET_NAME = module.images_bucket.aws_s3_bucket.bucket
        }
    }
    triggers = {
        always_run = "${timestamp()}"
    }
}

data "null_data_source" "wait_for_database_handler_function" {
  inputs = {
    lambda_exporter_id = "${null_resource.database_handler_function.id}"
    source_file = "${local.database_handler_root}/dist/index.js"
  }
}

data "archive_file" "database_handler_function_payload" {
    depends_on = [ data.null_data_source.wait_for_database_handler_function ]
    type             = "zip"
    source_file      = data.null_data_source.wait_for_database_handler_function.outputs.source_file
    output_file_mode = "0666"
    output_path      = "${local.database_handler_root}/dist/index.js.zip"
}

resource "aws_iam_policy" "dynamo_db_lambda_policy" {
  name = "dynamo-db-lambda-policy-${random_id.cd_function_suffix.hex}"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
            "dynamodb:Scan",
            "dynamodb:GetRecords",
            "dynamodb:GetItem",
            "dynamodb:PutItem",
            "dynamodb:UpdateItem",
            "dynamodb:DeleteItem",
            "dynamodb:GetShardIterator",
            "dynamodb:DescribeStream",
            "dynamodb:ListStreams"
        ]
        Resource = [
            aws_dynamodb_table.default.arn
        ]
      }
    ]
  })
}

resource "aws_iam_policy" "dynamo_db_lambda_policy_s3" {
  name = "dynamo-db-lambda-policy-s3-${random_id.cd_function_suffix.hex}"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "s3:PutObject",
          "s3:PutObjectAcl",
          "s3:GetObject",
          "s3:GetObjectAcl",
          "s3:DeleteObject",
          "s3:DeleteObjectAcl",
        ]
        Resource = [
            module.images_bucket.aws_s3_bucket.arn,
            "${module.images_bucket.aws_s3_bucket.arn}/*"
        ]
      }
    ]
  })
}

resource "aws_iam_role" "database_lambda_role" {
  name = "dynamo-access-role-${random_id.cd_function_suffix.hex}"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
        Action = "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_role_attachment" {
    role       = aws_iam_role.database_lambda_role.name
    policy_arn = aws_iam_policy.dynamo_db_lambda_policy.arn
}

resource "aws_iam_role_policy_attachment" "lambda_role_attachment_s3" {
    role       = aws_iam_role.database_lambda_role.name
    policy_arn = aws_iam_policy.dynamo_db_lambda_policy_s3.arn
}

module "database_handler_function" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "1.4.0"
    org              = var.resource_namespace
    project_name     = var.project_name
    lambda_payload   = data.archive_file.database_handler_function_payload
    function_name    = "db-handler-${random_id.cd_function_suffix.hex}"
    function_handler = "index.handler"
    publish          = true
    memory_size      = 512
    role             = aws_iam_role.database_lambda_role
    timeout          = 5
}

resource "aws_lambda_function_url" "database_handler_function_endpoint" {
    function_name      = module.database_handler_function.name
    authorization_type = "NONE"
}





# resource "aws_iam_user" "database_handler_user" {
#     name          = "db-handler-${random_id.cd_function_suffix.hex}"
#     path          = "/images-cms/"
#     force_destroy = true
# }

# resource "aws_iam_access_key" "database_handler_user_access_key" {
#     user = aws_iam_user.database_handler_user.name
# }

# data "aws_iam_policy_document" "database_handler_policy" {
#     statement {
#         actions   = [
#             "lambda:InvokeFunction",
#             "lambda:InvokeFunctionUrl",
#         ]
#         resources = [module.database_handler_function.invoke_arn]
#     }
# }

# resource "aws_iam_user_policy" "database_handler_role" {
#   name   = "db-handler-invoke-${random_id.cd_function_suffix.hex}"
#   user   = aws_iam_user.database_handler_user.name
#   policy = data.aws_iam_policy_document.database_handler_policy.json
# }

# output "db_user_access_key" {
#     value = aws_iam_access_key.database_handler_user_access_key
#     sensitive = true
# }
