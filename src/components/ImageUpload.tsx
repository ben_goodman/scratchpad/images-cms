import React, { useState, useCallback } from 'react'
import styled from 'styled-components'
import { Form, FormControl, TextInput } from '@contentful/f36-forms'
import { Button } from '@contentful/f36-button'
import { Stack } from '@contentful/f36-core'
import { StyledImage } from './StyledImage'

const ImageInput = styled.input`
    width: 100%;
    padding: 10px;
    box-sizing: border-box;
`

export interface ImageUploadProps {
    onCancel?: () => void
    onError?: (error: unknown) => void
    onSuccess?: () => void
}

export const ImageUpload = ({
    onCancel,
    onError,
    onSuccess
}: ImageUploadProps) => {
    const [image, setImage] = useState<File | null>(null)
    const [imageDescription, setImageDescription] = useState<string>('')
    const [imageTitle, setImageTitle] = useState<string>('')
    const [imageDimensions, setImageDimensions] = useState<[number, number] | null>(null)
    const [isSubmitButtonDisabled, setSubmitButtonDisabled] = useState<boolean>(false)

    const handleTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setImageTitle(e.target.value)
    }

    const handleDescriptionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setImageDescription(e.target.value)
    }

    const handleImageChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files) {
            const image = e.target.files[0]

            const img = new Image()
            img.src = URL.createObjectURL(image)

            img.onload = () => {
                setImageDimensions([img.width, img.height])
            }

            setImage(image)
            setImageTitle(image?.name || '')
        }
    },[])

    const handleSubmit = async () => {
        if (image && imageDimensions) {
            setSubmitButtonDisabled(true)
            const body = JSON.stringify({
                title: imageTitle,
                description: imageDescription,
                image: await image.arrayBuffer().then(buffer => Buffer.from(buffer).toString('base64')),
                dimensions: imageDimensions,
                type: image.type,
            })
            try {
                const url = `https://${process.env.API_DOMAIN}/api/image`
                await fetch(url, {
                    method: 'PUT',
                    body,
                })
                setSubmitButtonDisabled(false)
                onSuccess?.()
            } catch (err) {
                console.log(err)
                setSubmitButtonDisabled(false)
                onError?.(err)
            }
        }
    }

    return (
        <Form onSubmit={handleSubmit}>
            <FormControl isRequired>
                <FormControl.Label>Title</FormControl.Label>
                <TextInput
                    type="text"
                    value={imageTitle}
                    onChange={handleTitleChange}
                />
                <FormControl.HelpText>Enter the image&apos;s title.</FormControl.HelpText>
            </FormControl>

            <FormControl>
                <FormControl.Label>Description</FormControl.Label>
                <TextInput
                    type="text"
                    value={imageDescription}
                    onChange={handleDescriptionChange}
                />
                <FormControl.HelpText>Enter the image&apos;s description.</FormControl.HelpText>
            </FormControl>

            <FormControl isRequired>
                <FormControl.Label>File</FormControl.Label>
                <ImageInput
                    type="file"
                    accept="image/*"
                    onChange={handleImageChange}
                />
                <StyledImage
                    src={image && URL.createObjectURL(image) || undefined}
                    alt="preview"
                    width="400px"
                    height="300px"
                />
            </FormControl>

            <Stack>
                <Button isDisabled={isSubmitButtonDisabled} variant="primary" type="submit">Submit</Button>
                {onCancel && <Button variant="negative" onClick={onCancel}>Cancel</Button>}
            </Stack>
        </Form>
    )
}