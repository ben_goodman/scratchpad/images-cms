import { Image } from '@contentful/f36-image'
import styled from 'styled-components'

export const StyledImage = styled(Image)`
    border: 1px solid #ccc;
    border-radius: 4px;
    object-fit: scale-down;
`