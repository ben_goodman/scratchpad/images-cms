import React, { useEffect, useState } from 'react'
import { StyledImage } from './StyledImage'
import { Table } from '@contentful/f36-table'
import { AssetCollection, type DynamoScanResponse } from 'src/types'
import { DateTime } from '@contentful/f36-datetime'
// import { Button } from '@contentful/f36-button'
import InfiniteScroll from 'react-infinite-scroll-component'

const DEFAULT_LIMIT = 5

interface ImageDisplayProps {
    onRowClick?: (imageId: string) => void
}

export const TabulateImages = ({
    onRowClick
}: ImageDisplayProps) => {
    const [imageData, setImageData] = useState<AssetCollection>([])
    const [lastEvaluatedKey, setLastEvaluatedKey] = useState<string | undefined>(undefined)

    const fetchData = async () => {
        console.log('fetching data')
        const startFrom = lastEvaluatedKey ? `&from=${lastEvaluatedKey}` : ''
        const url = `https://${process.env.API_DOMAIN}/api/image?limit=${DEFAULT_LIMIT}${startFrom}`
        const response = await fetch(url)
        const data = await response.json() as DynamoScanResponse
        setImageData([...imageData, ...data.Items])
        setLastEvaluatedKey(data.LastEvaluatedKey?.id?.S)
    }

    useEffect(() => void fetchData(), [])

    return (
        <>
            <InfiniteScroll
                dataLength={imageData.length}
                next={fetchData}
                hasMore={Boolean(lastEvaluatedKey)}
                loader={<h4>Loading...</h4>}
            >
                <Table>
                    <Table.Head>
                        <Table.Row>
                            <Table.Cell>Image</Table.Cell>
                            <Table.Cell>Title</Table.Cell>
                            <Table.Cell>Dimensions</Table.Cell>
                            <Table.Cell>Type</Table.Cell>
                            <Table.Cell>Date Uploaded</Table.Cell>
                        </Table.Row>
                    </Table.Head>
                    <Table.Body>
                        {
                            imageData.map( image => {
                                return (
                                    <Table.Row key={image.id.S} onClick={() => onRowClick?.(image.id.S)}>
                                        <Table.Cell>
                                            <StyledImage
                                                src={`https://${process.env.API_DOMAIN}/${image.key.S}`}
                                                alt={image.title.S}
                                                width='300px'
                                                height='150px'
                                            />
                                        </Table.Cell>
                                        <Table.Cell>{image.title.S}</Table.Cell>
                                        <Table.Cell>{image.dimensions.L[0].N} x {image.dimensions.L[1].N}</Table.Cell>
                                        <Table.Cell>{image.type.S}</Table.Cell>
                                        <Table.Cell><DateTime date={image.dateUploaded.S}/></Table.Cell>
                                    </Table.Row>
                                )
                            })
                        }
                    </Table.Body>
                </Table>
            </InfiniteScroll>
            {/* <Button
                onClick={fetchData}
                isDisabled={!lastEvaluatedKey}
            >Load More</Button> */}
        </>
    )
}