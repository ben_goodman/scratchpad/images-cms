import { Button } from '@contentful/f36-button'
import { Flex } from '@contentful/f36-core'
import { Header } from '@contentful/f36-header'
import tokens from '@contentful/f36-tokens'
import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { deleteAsset } from 'src/services/deleteAsset'
import { useAssetData } from 'src/services/useAsset'
import { Notification } from '@contentful/f36-notification'
import { StyledImage } from 'src/components/StyledImage'
import { Form, FormControl, TextInput } from '@contentful/f36-forms'
import { CopyButton } from '@contentful/f36-copybutton'
import { updateAsset } from 'src/services/updateAsset'

export const Component = () => {
    const [deleteDisabled, setDeleteDisabled] = useState(false)
    const [assetDescription, setAssetDescription] = useState<string>('loading...')
    const [isUpdated, setIsUpdated] = useState(false)
    const { id } = useParams()
    const navigate = useNavigate()
    const assetData = useAssetData(id)

    useEffect(() => {
        if (assetData?.data?.description) {
            setAssetDescription(assetData?.data?.description)
        }
    }, [assetData?.data?.description])

    const handleAssetDeletion = async () => {
        setDeleteDisabled(true)
        deleteAsset(id)
            .catch(() => Notification.error('Failed to delete asset.'))
            .then(() => {
                Notification.success('Asset deleted.')
                navigate('../images')
            })
            .finally(() => setDeleteDisabled(false))
    }

    const handleAssetDescriptionChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAssetDescription(e.target.value)
        setIsUpdated(true)
    }

    const handleAssetUpdate = () => {
        if (id) {
            updateAsset(id, {description: assetDescription})
                .finally(() => {
                    console.log('updated')
                    setIsUpdated(false)
                })
        }
    }

    return (
        <>
            <Header
                title={id}
                withBackButton={true}
                backButtonProps={{ onClick: () => navigate('/') }}
                breadcrumbs={[
                    {
                        content: 'Assets',
                        url: '/images',
                    },
                ]}
                actions={
                    <Flex
                        alignItems="center"
                        gap={tokens.spacingS}
                        justifyContent="flex-end"
                    >
                        <Button
                            isDisabled={!isUpdated}
                            variant='positive'
                            onClick={handleAssetUpdate}
                        >
                            Save Changes
                        </Button>
                        <Button
                            isDisabled={deleteDisabled}
                            onClick={handleAssetDeletion}
                            variant='negative'
                        >
                            Delete Asset
                        </Button>
                    </Flex>
                }
            />


            <Form>
                <FormControl>
                    <FormControl.Label>Asset ID</FormControl.Label>

                    <TextInput.Group>
                        <TextInput isDisabled isReadOnly value={assetData?.data?.id} />
                        <CopyButton
                            value={assetData?.data?.id || ''}
                            tooltipProps={{ placement: 'right', usePortal: true }}
                        />
                    </TextInput.Group>

                </FormControl>

                <FormControl>
                    <FormControl.Label>Description</FormControl.Label>
                    <TextInput
                        isDisabled={assetData?.loading}
                        value={assetDescription}
                        onChange={handleAssetDescriptionChange}
                    />
                </FormControl>

                <FormControl isReadOnly>
                    <FormControl.Label>Asset</FormControl.Label>
                    <StyledImage
                        src={assetData && `https://${process.env.API_DOMAIN}/${assetData?.data?.key}` || undefined}
                        alt={assetData?.data?.title || 'loading'}
                        width="500px"
                        height="500px"
                    />
                </FormControl>

                <FormControl isReadOnly isDisabled >
                    <FormControl.Label>Dimensions</FormControl.Label>
                    <TextInput
                        type='text'
                        value={`${assetData?.data?.dimensions[0]} x ${assetData?.data?.dimensions[1]}`}
                    />
                </FormControl>

                <FormControl isReadOnly isDisabled>
                    <FormControl.Label>Type</FormControl.Label>
                    <TextInput value={assetData?.data?.type}/>
                </FormControl>

            </Form>
        </>
    )
}

Component.displayName = 'EditImage'