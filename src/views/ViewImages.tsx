import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { Button } from '@contentful/f36-button'
import { Modal } from '@contentful/f36-modal'
import { ImageUpload } from 'src/components/ImageUpload'
import { TabulateImages } from 'src/components/TabulateImages'
import { Header } from '@contentful/f36-header'
import { Flex } from '@contentful/f36-core'
import tokens from '@contentful/f36-tokens'
import { Notification } from '@contentful/f36-notification'

export const Component = () => {
    const navigate = useNavigate()
    const [newImageModalOpen, setNewImageModalOpen] = useState(false)

    return (
        <>
            <Header
                title="View Assets"
                actions={
                    <Flex
                        alignItems="center"
                        gap={tokens.spacingS}
                        justifyContent="flex-end"
                    >
                        <Button
                            onClick={() => setNewImageModalOpen(true)}
                            variant='primary'
                        >
                        Upload Asset
                        </Button>
                    </Flex>
                }
            />


            <Modal onClose={() => setNewImageModalOpen(false)} isShown={newImageModalOpen}>
                {() => (
                    <>
                        <Modal.Header
                            title="Upload Asset"
                            onClose={() => setNewImageModalOpen(false)}
                        />
                        <Modal.Content>
                            <ImageUpload
                                onSuccess={() => {setNewImageModalOpen(false);Notification.success('Image uploaded.')}}
                                onError={() => {setNewImageModalOpen(false);Notification.error('Image upload failed.')}}
                            />
                        </Modal.Content>
                    </>
                )}
            </Modal>

            <TabulateImages onRowClick={(id) => navigate(`./image/${id}`)} />

        </>
    )
}

Component.displayName = 'ViewImages'