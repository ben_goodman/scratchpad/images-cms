export const deleteAsset = async (assetId?: string) => {
    if (!assetId) {
        throw new Error('No asset id provided')
    }
    const url = `https://${process.env.API_DOMAIN}/api/image/${assetId}`
    return fetch(url, { method: 'DELETE' })
}