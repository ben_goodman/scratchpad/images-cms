export interface UpdateAssetOpts {
    description: string
}

export const updateAsset = (assetId: string, opts: UpdateAssetOpts) => {
    const body = JSON.stringify(opts)
    const url = `https://${process.env.API_DOMAIN}/api/image/${assetId}`
    return fetch(url, {
        method: 'PUT',
        body
    })
}
