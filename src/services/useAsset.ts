import { useState, useEffect } from 'react'
import { type AssetData } from 'src/types'


export interface AssetDataRequest {
    loading: boolean
    error: boolean
    data: AssetData | undefined
}

const DEFAULT_REQUEST = {
    loading: true,
    error: false,
    data: undefined,
}

export const useAssetData = (assetId?: string) => {
    const [assetDataRequest, setAssetDataRequest]
        = useState<AssetDataRequest|undefined>(DEFAULT_REQUEST)

    useEffect(() => {
        const fetchAssets = async () => {
            try {
                const url = `https://${process.env.API_DOMAIN}/api/image/${assetId}`
                const response = await fetch(url)
                const data = await response.json() as AssetData
                setAssetDataRequest({
                    loading: false,
                    error: false,
                    data,
                })
            } catch (err) {
                console.error(err)
                setAssetDataRequest({
                    loading: false,
                    error: true,
                    data: undefined,
                })
            }
        }

        if (assetId) {
            void fetchAssets()
        }
    }, [])

    return assetDataRequest
}