export interface DynamoString {
    S: string
}

export interface DynamoNumber {
    N: number
}

export interface DynamoList<T> {
    L: Array<T>
}

export interface AssetEntry {
    description: DynamoString
    id: DynamoString
    key: DynamoString
    title: DynamoString
    dimensions: DynamoList<DynamoNumber>
    type: DynamoString
    dateUploaded: DynamoString
}

export type AssetCollection = Array<AssetEntry>

export interface AssetData {
    description: string
    id: string
    key: string
    title: string
    dimensions: number[]
    type: string
    dateUploaded: string
}

export interface DynamoScanResponse {
    $metadata: {
        httpStatusCode: number,
        requestId: string,
        extendedRequestId: string|undefined,
        cfId: string|undefined,
        attempts: number,
        totalRetryDelay: number,
    }
    Items: AssetCollection
    Count: number
    ScannedCount: number
    LastEvaluatedKey: {id: DynamoString}
}

